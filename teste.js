const express = require('express')
const app = express();
const Sequelize = require('sequelize')

const sequelize = new Sequelize('blog', 'root', '',{
    host: 'localhost',
    dialect: 'mysql'
})

const Usuario = sequelize.define('usuarios',{
    nome: {
        type: Sequelize.STRING
    },

    nascimento: {
        type: Sequelize.DATEONLY
    },

    email: {
        type: Sequelize.STRING
    }
});

Usuario.create( 
{
    nome: "Aldo Emidio Nhaca",
    nascimento: "2001-01-05:03:05:47",
    email: "aldoemidio@gmail.com"
}
)

// Usuario.sync({force:true});


sequelize.authenticate().then(()=>{
    console.log('MySQL Server connected successfully');
}).catch((erro)=>{
    console.log('Connection Failed...');
})
app.listen(8001, function(){
    console.log('Servidor conectado com sucesso!')
})
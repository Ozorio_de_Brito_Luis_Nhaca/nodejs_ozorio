const express = require('express')
const app = express();
const Sequelize = require('sequelize')
const exphbs = require('express-handlebars')

const sequelize = new Sequelize('blog', 'root', '',{
    host: 'localhost',
    dialect: 'mysql'
})

//Config
    //Tamplate engine
    app.engine('hbs', exphbs({
        defaultLayout: 'main',
        extname: '.hbs'
    }));
    
    app.set('view engine', 'hbs');


// Usuario.sync({force:true});
sequelize.authenticate().then(()=>{
    console.log('MySQL Server connected successfully');
}).catch((erro)=>{
    console.log('Connection Failed...');
})
app.listen(8001, function(){
    console.log('Servidor conectado com sucesso!')
})
